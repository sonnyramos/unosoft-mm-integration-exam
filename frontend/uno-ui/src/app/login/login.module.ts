import { LoginRouting } from './login.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';

import { A2tUiModule } from 'angular2-token';

@NgModule({
  imports: [
    CommonModule,
    LoginRouting,
    FormsModule,
    ReactiveFormsModule,
    A2tUiModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
