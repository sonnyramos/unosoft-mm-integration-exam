import { HttpService } from './../pages/@services/http.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [HttpService]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  output: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpService,
  ) {
    this.loginForm = fb.group({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
  }

  login() {
    console.log(this.loginForm.value);

    this.http.login(this.loginForm.value).subscribe(response => {
      var _body = response.json();

      if (_body.status == 'success') {
        sessionStorage.setItem('access_token', _body.access_token);
        this.router.navigate(['/pages']);
      } else {
        sessionStorage.clear();
      }
    })
  }

}
