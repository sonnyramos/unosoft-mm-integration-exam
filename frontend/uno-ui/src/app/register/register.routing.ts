import { RegisterComponent } from './register.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
    {
        path: '',
        component: RegisterComponent
    }
];

export const RegisterRouting: ModuleWithProviders = RouterModule.forChild(routes);
