import { HttpService } from './../pages/@services/http.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [HttpService]
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  passwordMSG;
  disabled = true;
  passwordClass;

  readonly = false;

  confirmpassword

  constructor(private fb: FormBuilder, private router: Router, private http: HttpService) {
    this.resetFields();
  }

  resetFields() {
    this.registerForm = this.fb.group({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      preferred_name: new FormControl('', Validators.required),
      mobile_country_code: new FormControl('', Validators.required),
      mobile: new FormControl('', Validators.required),
      device_signature: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    })
    this.confirmpassword = '';
  }


  ngOnInit() {
  }

  register() {
    console.log(JSON.stringify(this.registerForm.value))

    this.passwordMSG = 'Registering...';
    this.passwordClass = 'mute';
    this.disabled = true;
    this.readonly = true;

    this.http.register(this.registerForm.value).subscribe(res => {
      var result = res.json();
      console.log(res)
      if (result.status == 'success') {
        this.passwordMSG = 'SUCCESSFULLY REGISTERED!';
        this.passwordClass = 'success';
        sessionStorage.setItem('access_token', result.access_token)
        this.resetFields();
      } else {
        this.passwordMSG = 'Unable to register account.';
        this.passwordClass = 'danger';
      }

      this.readonly = false;
    })
  }

  passwordMatching() {
    var register = this.registerForm.value;

    console.log(this.disabled);

    if (register.password == this.confirmpassword) {
      this.passwordMSG = 'Password Match';
      this.passwordClass = 'success';
      this.disabled = false;
    } else {
      this.passwordMSG = 'Password do not match.';
      this.passwordClass = 'danger';
      this.disabled = true;
    }
  }

}
