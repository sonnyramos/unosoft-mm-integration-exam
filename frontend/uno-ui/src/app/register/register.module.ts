import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterRouting } from './register.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import { ReCaptchaModule } from 'angular2-recaptcha';

@NgModule({
  imports: [
    CommonModule,
    RegisterRouting,
    FormsModule,
    ReactiveFormsModule,
    ReCaptchaModule
  ],
  declarations: [RegisterComponent]
})
export class RegisterModule { }
