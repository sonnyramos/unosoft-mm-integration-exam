import { HttpService } from './../@services/http.service';
import { Component, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
  providers: [HttpService]
})
export class CheckoutComponent implements OnInit {

  saving = false;


  amount = 0;

  message: string;
  msgClass;

  constructor(private _zone: NgZone, private http: HttpService) { }

  ngOnInit() {
    this.resetFields()
  }

  resetFields() {

    this.amount = 0;
  }

  getToken() {

    if (this.fieldValid()) {
      this.message = 'Loading...';

      this.saving = true;

      var data = {
        access_token: sessionStorage.getItem('access_token'),
        currency: 'sgd',
        amount: (this.amount).toString()
      }
      console.log(data)

      this.http.cashtopup(data).subscribe(resp => {

        var dat = resp.json();
        console.log(dat)

        if (dat.status == 'success') {
          this.resetFields();
          this.message = 'Transaction is successful!';
          this.msgClass = 'success';
        } else {
          this.message = 'Transaction is unsuccessful!';
          this.msgClass = 'danger';
        }

        this.saving = false;

      });
    }
  }

  fieldValid() {
    return (this.amount && this.amount >= 0);
  }

}
