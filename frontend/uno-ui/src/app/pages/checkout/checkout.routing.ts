import { CheckoutComponent } from './checkout.component';

import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
    {
        path: '',
        component: CheckoutComponent
    }
];

export const CheckoutRouting: ModuleWithProviders = RouterModule.forChild(routes);
