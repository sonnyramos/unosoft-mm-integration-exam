import { FormsModule, ReactiveFormsModule, PatternValidator } from '@angular/forms';
import { CheckoutRouting } from './checkout.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutComponent } from './checkout.component';

@NgModule({
  imports: [
    CommonModule,
    CheckoutRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [CheckoutComponent]
})
export class CheckoutModule { }
