import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
  private api = 'https://uno-rails-app.herokuapp.com/api/v1/';
  constructor(private http: Http) { }

  register(data) {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.api + 'users', data, options)
      .map(map => {
        console.log(map);
        return map;
      });
  }

  updateDetails(data) {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    return this.http.put(this.api + 'users', data, options)
      .map(map => {
        console.log(map);
        return map;
      });
  }

  login(data) {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.api + 'auth', data, options)
      .map(map => {
        console.log(map);
        return map;
      });
  }

  userInfo(data) {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    return this.http.get(this.api + 'users?access_token=' + data.access_token, options)
      .map(map => {
        console.log(map);
        return map;
      });
  }

  cashtopup(data) {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.api + 'payments', data, options)
      .map(map => {
        console.log(map);
        return map;
      });
  }

  cardtopup(data) {
    const headers = new Headers({ "Content-Type": "application/json" });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.api + 'callback/stripe', data, options)
      .map(map => {
        console.log(map);
        return map;
      });
  }
}

