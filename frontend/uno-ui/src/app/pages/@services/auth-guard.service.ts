import { Router, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuardService {

  constructor(private router: Router) { }

  canActivate() {
    if (sessionStorage.getItem('access_token')) {
      // logged in so return true

      // add http. get user to check if access_token is not yet expired
      return true;
    }

    // not logged in so redirect to login page
    this.router.navigate(['/login']);
    return false;

  }

}
