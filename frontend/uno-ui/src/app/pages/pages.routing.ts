import { AuthGuardService } from './@services/auth-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
    {
        path: 'login',
        loadChildren: '../login/login.module#LoginModule'
    },
    {
        path: 'register',
        loadChildren: '../register/register.module#RegisterModule'
    },
    {
        path: 'pages',
        component: PagesComponent,
        canActivate: [AuthGuardService],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'cash', loadChildren: './checkout/checkout.module#CheckoutModule' },
            { path: 'account', loadChildren: './account/account.module#AccountModule' },
            { path: 'card', loadChildren: './checkout-card/checkout-card.module#CheckoutCardModule' },
        ]
    }
];

export const PagesRouting: ModuleWithProviders = RouterModule.forChild(routes);
