import { CheckoutCardComponent } from './checkout-card.component';

import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
    {
        path: '',
        component: CheckoutCardComponent
    }
];

export const CheckoutCardRouting: ModuleWithProviders = RouterModule.forChild(routes);
