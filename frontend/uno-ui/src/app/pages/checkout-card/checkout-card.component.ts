import { HttpService } from './../@services/http.service';
import { Component, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-checkout-card',
  templateUrl: './checkout-card.component.html',
  styleUrls: ['./checkout-card.component.css'],
  providers: [HttpService]
})
export class CheckoutCardComponent implements OnInit {

  cardNumber: string;
  expiry: string;
  cvc: string;

  email;
  device_signature = 'IOS 11';

  amount = 0;

  message: string;
  msgClass;

  constructor(private _zone: NgZone, private http: HttpService) { }

  ngOnInit() {
    this.resetFields()
  }

  resetFields() {
    this.cardNumber = '';
    this.expiry = '';
    this.cvc = '';

    this.amount = 0;
    this.message = '';
  }

  getToken() {

    if (!this.fieldValid())
      this.message = 'Loading...';

    var temp = this.expiry.split('/');

    var expiryMonth = temp[0];
    var expiryYear = temp[1];

    (<any>window).Stripe.card.createToken({
      number: this.cardNumber,
      exp_month: expiryMonth,
      exp_year: expiryYear,
      cvc: this.cvc
    }, (status: number, response: any) => {

      // Wrapping inside the Angular zone
      this._zone.run(() => {
        if (status === 200) {
          // payment method here
          var data = {
            access_token: sessionStorage.getItem('access_token')
          }
          this.http.userInfo(data).subscribe(results => {

            console.log(results.json())

            if (results.json().status == 'success') {

              var data = {
                access_token: sessionStorage.getItem('access_token'),
                stripeToken: response.id,
                email: results.json().user.email,
                device_signature: this.device_signature,
                currency: 'sgd',
                amount: (this.amount).toString()
              }
              console.log(response)
              console.log(data)

              this.http.cardtopup(data).subscribe(resp => {
                console.log(response)
                console.log(resp)
                var data = resp.json();
                if (data.status == 'success') {
                  this.resetFields();
                  this.message = 'Transfer of fund is successful!';
                  this.msgClass = 'success';
                } else {
                  this.message = 'Transaction unsuccessful!';
                  this.msgClass = 'danger';
                }
              })
            }
            else {
              this.message = 'Account is suspected to be expired';
              this.msgClass = 'danger';
            }
          })

        } else {
          this.message = response.error.message;
        }
      });
    });
  }

  fieldValid() {
    return (this.amount && this.amount <= 0 && this.cardNumber && this.cvc && this.expiry);
  }

}
