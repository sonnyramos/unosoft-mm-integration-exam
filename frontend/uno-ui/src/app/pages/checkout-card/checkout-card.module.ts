import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckoutCardRouting } from './checkout-card.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutCardComponent } from './checkout-card.component';

@NgModule({
  imports: [
    CommonModule,
    CheckoutCardRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [CheckoutCardComponent]
})
export class CheckoutCardModule { }
