import { AccountComponent } from './account.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
    {
        path: '',
        component: AccountComponent
    }
];

export const AccountRouting: ModuleWithProviders = RouterModule.forChild(routes);
