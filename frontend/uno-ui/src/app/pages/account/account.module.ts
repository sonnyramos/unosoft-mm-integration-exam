import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountRouting } from './account.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';

@NgModule({
  imports: [
    CommonModule,
    AccountRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AccountComponent]
})
export class AccountModule { }
