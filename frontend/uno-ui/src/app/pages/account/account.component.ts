import { HttpService } from '../../pages/@services/http.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [HttpService]
})
export class AccountComponent implements OnInit {

  userDetails: FormGroup

  editMode = false;

  read_only = true;

  user;

  original;
  msgClass;
  message;

  constructor(private http: HttpService, private fb: FormBuilder) { }

  ngOnInit() {
    this.userDetails = this.fb.group({
      access_token: sessionStorage.getItem('access_token'),
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      preferred_name: new FormControl('', Validators.required),
      mobile_country_code: new FormControl('', Validators.required),
      mobile: new FormControl('', Validators.required),
      device_signature: new FormControl(''),
      email: new FormControl('', Validators.required),
      title: new FormControl(''),
    })
    this.fetch();
  }

  submitChanges() {
    this.msgClass = '';
    this.message = '';
    console.log(this.userDetails.value);
    var data = {
      first_name: this.userDetails.value.first_name,
      last_name: this.userDetails.value.last_name,
      preferred_name: this.userDetails.value.preferred_name,
      email: this.userDetails.value.email,
      title: this.userDetails.value.title,
      access_token: sessionStorage.getItem('access_token')
    }

    console.log(data);

    this.http.updateDetails(JSON.stringify(this.userDetails.value)).subscribe(res => {
      var original = res.json();
      console.log(original)
      if (original.status == 'success') {
        this.msgClass = '';
        this.message = '';
        this.editMode = !this.editMode;
        this.read_only = !this.read_only;
        this.fetch();

        this.msgClass = 'success';
        this.message = 'Changes successfully applied';
      } else {
        this.msgClass = 'danger';
        this.message = 'Error occurs. Try again.';
      }
    })
  }

  toggleEdit() {
    this.msgClass = '';
    this.message = '';
    this.editMode = !this.editMode;
    this.read_only = !this.read_only;
    if (!this.editMode) {
      this.resetDefault();
    }
  }

  fetch() {

    var data = {
      access_token: sessionStorage.getItem('access_token')
    }
    this.http.userInfo(data).subscribe(response => {
      this.original = response.json();

      console.log(this.original)

      if (this.original.status == 'success') {
        this.userDetails = this.fb.group({
          access_token: sessionStorage.getItem('access_token'),
          first_name: new FormControl(this.original.user.name.first, Validators.required),
          middle_name: new FormControl(this.original.user.name.middle),
          last_name: new FormControl(this.original.user.name.last, Validators.required),
          preferred_name: new FormControl(this.original.user.name.preferred, Validators.required),
          mobile_country_code: new FormControl(this.original.user.mobile.country_code, Validators.required),
          mobile: new FormControl(this.original.user.mobile.number, Validators.required),
          device_signature: new FormControl(''),
          email: new FormControl(this.original.user.email, Validators.required),
          title: new FormControl(this.original.user.title || ''),
        })
      }
    })
  }

  resetDefault() {
    this.userDetails = this.fb.group({
      access_token: sessionStorage.getItem('access_token'),
      first_name: new FormControl(this.original.user.name.first, Validators.required),
      middle_name: new FormControl(this.original.user.name.middle),
      last_name: new FormControl(this.original.user.name.last, Validators.required),
      preferred_name: new FormControl(this.original.user.name.preferred, Validators.required),
      mobile_country_code: new FormControl(this.original.user.mobile.country_code, Validators.required),
      mobile: new FormControl(this.original.user.mobile.number, Validators.required),
      device_signature: new FormControl(''),
      email: new FormControl(this.original.user.email, Validators.required),
      title: new FormControl(this.original.user.title || ''),
    })

  }

}
