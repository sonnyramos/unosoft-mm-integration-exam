import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
    {
        path: '',
        component: DashboardComponent
    }
];

export const DashboardRouting: ModuleWithProviders = RouterModule.forChild(routes);
