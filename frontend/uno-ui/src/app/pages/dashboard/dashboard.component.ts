import { HttpService } from '../../pages/@services/http.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [HttpService]
})
export class DashboardComponent implements OnInit {

  data;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.resetDefault()
  }

  submitChanges() {

  }

  resetDefault() {
    var data = {
      access_token: sessionStorage.getItem('access_token')
    }
    this.http.userInfo(data).subscribe(response => {

      console.log(response.json())

      if (response.json().status == 'success') {
        this.data = response.json();
      }
    })

  }

}
