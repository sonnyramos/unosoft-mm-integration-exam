import { AuthGuardService } from './@services/auth-guard.service';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { PagesRouting } from './pages.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { HeaderComponent } from './@component/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    PagesRouting,
    RouterModule,
    HttpModule
  ],
  providers: [AuthGuardService],
  declarations: [PagesComponent, HeaderComponent]
})
export class PagesModule { }
