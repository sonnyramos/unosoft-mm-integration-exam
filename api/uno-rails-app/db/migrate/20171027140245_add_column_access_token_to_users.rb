class AddColumnAccessTokenToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :access_token, :string, :length => 25
  end
end
