class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.integer :user_id
      t.string :card_id
      t.decimal :amount
      t.string :currency
      t.string :description
      t.string :stripeToken
      t.string :status #pending, #paid, #topupped
      t.timestamps
    end
  end
end
