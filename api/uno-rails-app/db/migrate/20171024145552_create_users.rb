class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :last_name, :required => true, :length => 25
      t.string :middle_name, :required => true, :length => 25
      t.string :first_name, :required => true, :length => 25
      t.string :email, :required => true, :length => 25
      t.string :password, :required => true, :length => 25
      t.string :preferred_name, :required => true, :length => 25
      t.string :mobile_country_code, :required => true, :length => 3
      t.string :mobile, :required => true, :length => 12
      t.string :device_signature, :required => true, :length => 25
      t.string :status, default: "pending", :length => 10

      t.timestamps
    end
  end
end
