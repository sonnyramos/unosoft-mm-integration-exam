class AlterColumnAmountInPayments < ActiveRecord::Migration[5.1]
  def change
    change_column :payments, :amount, :integer
  end
end
