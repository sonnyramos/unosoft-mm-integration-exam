class AddIndexUniqueEmailAndMobileNumber < ActiveRecord::Migration[5.1]
  def change
    add_index :users, :email, :unique => true
    add_index :users, [:mobile_country_code, :mobile], :unique => true
  end
end
