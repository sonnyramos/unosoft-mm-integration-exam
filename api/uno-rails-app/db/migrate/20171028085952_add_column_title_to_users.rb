class AddColumnTitleToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :title, :string, :length => 10
  end
end
