module UserApi
  extend ActiveSupport::Concern

  def api_login

    conn = self.connect
    conn.get do |req|
      req.url '/api/v1/users'
      req.headers['Authorization'] = "Bearer #{self.access_token}"
      req.headers['Content-Type'] = "application/x-www-form-urlencoded"
      req.body = URI.encode_www_form({"grant_type" => "client_credentials"})
    end
  end

  def api_create
    conn = self.connect_with_auth
    response = conn.post do |req|
      req.url '/api/v1/users'
      req.body = sanitize(self.attributes.compact!).merge!("grant_type" => "client_credentials")
    end

    if response.status != 200
      raise "Error"
    else
      body = JSON.parse(response.body)
      self.access_token = body["access_token"]
    end

  end

  def api_update
    return if self.changed_attributes == {}
    conn = self.connect
    response = conn.put do |req|
      req.url '/api/v1/users'
      req.headers['Authorization'] = "Bearer #{self.access_token}"
      req.headers['Content-Type'] = "application/x-www-form-urlencoded"
      data = sanitize(self.attributes, sanitize(self.changed_attributes))
      req.body = URI.encode_www_form(data)

    end
    if response.status != 200
      raise "Error"
    end
  end



  def api_info
    conn = self.connect
    response = conn.get do |req|
      req.url '/api/v1/users'
      req.headers['Authorization'] = "Bearer #{self.access_token}"
    end
    if response.status != 200
      raise "Error"
    end
    JSON.parse(response.body)
  end


  def api_wallet_info

    conn = self.connect
    response  = conn.get do |req|
      req.url '/api/v1/users/wallets'
      req.headers['Authorization'] = "Bearer #{self.access_token}"

    end

    JSON.parse(response.body)
  end

  def connect_with_auth
    Faraday.new(:url => ENV['matchmove_base_uri']) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      faraday.basic_auth(ENV['username'], ENV['password'])
    end
  end

  def connect
    Faraday.new(:url => ENV['matchmove_base_uri']) do |faraday|
      # faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
  end

  def sanitize(params, keys = nil)
    if keys.present?
      valid_keys = keys
    else
      valid_keys = ['email', 'password', 'first_name', 'last_name', 'preferred_name', 'mobile_country_code',
                  'mobile', 'device_signature', "grant_type", "title"]
    end
    params.select {|key, value| valid_keys.include?(key) }
  end

end

