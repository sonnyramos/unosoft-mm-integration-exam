module PaymentApi
  extend ActiveSupport::Concern



  def api_create
    conn = self.connect_with_auth
    response = conn.post do |req|
      req.url '/api/v1/users/wallets/funds'
      req.body = {"email" => self.user.email, "amount" => (self.amount)}
    end

    if response.status != 200
      raise "Error"
    else
      self.update(status: "topupped")
    end

  end


  def connect_with_auth
    Faraday.new(:url => ENV['matchmove_base_uri']) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      faraday.basic_auth(ENV['username'], ENV['password'])
    end
  end


  def connect
    Faraday.new(:url => ENV['matchmove_base_uri']) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
  end



end