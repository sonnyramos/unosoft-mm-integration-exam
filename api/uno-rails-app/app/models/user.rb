class User < ApplicationRecord

  include UserApi

  validates :email, :uniqueness => true
  validates :mobile_country_code, uniqueness: { scope: :mobile }


  before_create :api_create
  before_update :api_update

  has_many :payments

end
