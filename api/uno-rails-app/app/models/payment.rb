class Payment < ApplicationRecord
  include PaymentApi
  belongs_to :user
  after_create :api_create


  def charge_stripe
    response  = Stripe::Charge.create(
        :amount => self.amount * 100,
        :currency => self.currency,
        :source => self.stripeToken, # obtained with Stripe.js
        :description => "Charge for #{self.user.email}"
    )
    #sk_test_7FM68CDkbo5fpsBiBudyqC4h
    if response[:paid]
      #call topup api
      self.status = "paid"
    end
  end
end
