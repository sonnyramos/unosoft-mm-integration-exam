class Api::V1::PaymentsController < ApplicationController


  def index
    User.all
  end

  def callback
    #stripe
    user = User.where(access_token: params[:access_token]).last
    payment = user.payments.create(payments_params)
    payment.send("charge_#{params[:provider]}")
    begin
      Payment.transaction do
        payment.save
      end
      render json: {status: :success, access_token: user.access_token, payment: payment}
    rescue RuntimeError
      render json: {status: :failed}
    end
  end

  def create
    user = User.where(access_token: params[:access_token]).last
    payment = user.payments.create(payments_params)
    begin
      Payment.transaction do
        payment.save
      end
      render json: {status: :success, access_token: user.access_token, payment: payment}
    rescue RuntimeError
      render json: {status: :failed}
    end
  end


  private

  def payments_params
    params.permit(:amount, :currency, :stripeToken)
  end
end