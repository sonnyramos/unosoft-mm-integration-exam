class Api::V1::TokensController < ApplicationController
  def create
    user = User.where(email: params[:email]).last

    begin
      user = User.where(email: params[:email]).last
      raise "error" unless (user.present? and user.password == params[:password])
      render json: {status: :success, access_token: user.access_token, user: user.api_info}
    rescue RuntimeError
      render json: {status: :failed}
    end
  end
end