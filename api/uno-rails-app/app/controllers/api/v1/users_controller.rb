class Api::V1::UsersController < ApplicationController


  def show
    begin
      user = User.where(access_token: params[:access_token]).last
      render json: {status: :success, access_token: user.access_token, user: user.api_info, wallet: user.api_wallet_info}
    rescue RuntimeError
      render json: {status: :failed}
    end
  end

  def create
    begin
    user = User.new(user_params)
    User.transaction do
      user.save!
    end
    render json: {status: :success, access_token: user.access_token}
    rescue RuntimeError
      render json: {status: :failed}
    end
  end

  def update
    begin
      user = User.where(access_token: params[:access_token]).last
      User.transaction do
        user.update(user_params)
      end
      render json: {status: :success, access_token: user.access_token}
    rescue RuntimeError
      render json: {status: :failed}
    end
  end

  private

  def user_params
    params.permit(:email, :password, :last_name, :first_name,
                                 :middle_name, :mobile_country_code, :mobile, :preferred_name, :device_signature, :title)
  end


end
