Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #login
  #user login
  namespace :api, :defaults => {:format => :json} do
    namespace :v1 do
      post "auth", :controller => :tokens, :action => :create


      # post :users , :controller => :users, :action => :create
      put :users , :controller => :users, :action => :update
      get :users , :controller => :users, :action => :show
      match 'users', to: 'users#create' , via: [ :post]
      match '*path', via: [:options], to: lambda {|_| [204, { 'Access-Control-Allow-Origin' => "https://uno-ui.herokuapp.com", 'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept, Authorization, Token','Content-Type' => 'text/plain' }]}

      post :payments, :controller => :payments, :action => :create
      post "callback/:provider", :controller => :payments, :action => :callback
    end
  end
end
